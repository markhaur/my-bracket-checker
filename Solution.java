import java.util.*;
class Solution{
	
	public static void main(String []argh)
	{
		Scanner sc = new Scanner(System.in);
		String input =  "";
		while (sc.hasNext()) {
			input = sc.next();
			Stack<String> straight = new Stack<>();
			Stack<String> temp = new Stack<>();
			String[] strings = input.split("");
			if(strings.length % 2 != 0){
				System.out.printf("%s%n", "false");
				continue;
			}
			for(String str : strings){
				straight.push(str);
			}
			while(!straight.empty()){
				String sPop = straight.pop();
				if(temp.empty() || !((sPop.equals("{") && temp.peek().equals("}")) ||
						(sPop.equals("[") && temp.peek().equals("]")) ||
						(sPop.equals("(") && temp.peek().equals(")")))){
					temp.push(sPop);
				}
				else{
					temp.pop();
				}
			}
			if(temp.empty()){
				System.out.printf("%s%n", "true");
			}
			else{
				System.out.printf("%s%n", "false");
			}
		}
		
	}
}




